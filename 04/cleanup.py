
import fileinput
from typing import Iterable

class Range:

    rmin: int
    rmax: int
    length: int

    def __init__(self, rang: str):
        self.rmin,self.rmax = map(lambda x: int(x), rang.strip().split('-'))
        self.length = self.rmax - self.rmin + 1


def solve(lines: Iterable[str]) -> int:
    score = 0
    for line in lines:
        elf1, elf2 = map(lambda x: Range(x), line.strip().split(','))
        if elf2.length > elf1.length:
            elf2,elf1 = (elf1, elf2)
        if elf1.rmin <= elf2.rmin and elf1.rmax >= elf2.rmax:
            score += 1
    return score

def solveb(lines: Iterable[str]) -> int:
    score = 0
    for line in lines:
        elf1, elf2 = map(lambda x: Range(x), line.strip().split(','))
        if elf2.length > elf1.length:
            elf2,elf1 = (elf1, elf2)
        if elf1.rmin <= elf2.rmax and elf1.rmax >= elf2.rmin:
            score += 1
    return score


if __name__ == "__main__":
    print(solve(fileinput.input()))
    print(solveb(fileinput.input()))