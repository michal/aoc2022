
import fileinput
from typing import Iterable

def solve(lines: Iterable[str]) -> int:
  max_calories = 0
  current_elf = 0
  for line in lines:
    if line == "\n":
      # End of current elf
      max_calories = max(max_calories, current_elf)
      current_elf = 0
    else:
      current_elf += int(line)
  return max_calories

def solveb(lines: Iterable[str]) -> int:
  max_calories = [0,0,0]
  current_elf = 0
  for line in lines:
    if line == "\n":
      # End of current elf
      max_calories = sorted(max_calories + [current_elf])[1:4]
      current_elf = 0
    else:
      current_elf += int(line)
  return sum(max_calories)

if __name__ == "__main__":
  print("Maximum calories: ", solve(fileinput.input()))
  print("Top 3: ", solveb(fileinput.input()))
