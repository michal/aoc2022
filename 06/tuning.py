
import fileinput
from typing import Iterable

def solve(data: str, length: int = 4) -> int:
    for idx in range(len(data)):
        if len(set(list(data[idx:idx+length]))) == length:
            return idx + length

if __name__ == "__main__":
    with fileinput.input() as f:
        data = f.readline()
    print(solve(data, 4))
    print(solve(data, 14))
